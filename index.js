#!/usr/bin/env node

const fs = require('fs');
const axios = require('axios');
const CLI = require('clui');
const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
const files = require('./lib/files');
const inquirer = require('inquirer');

const Table = require('cli-table');
const Spinner = CLI.Spinner;

const homedir =
  process.platform === 'win32'
    ? process.env.HOMEPATH
    : `${process.env.HOME}/Downloads/`;

clear();
console.log(chalk.yellow(figlet.textSync('Sherlock', { width: '50' })));

const status = new Spinner('Waiting...');
const infoOuput = [];
const errors = [];

// build table
const table = new Table({
  head: ['wmPackageId', 'wmShipmentId', 'carrierTrackingId'],
  colWidths: [20, 24, 24],
});

const parseText = (arr) => {
  return arr.trim().split(/\r?\n/);
};

// cli prompts

inquirer
  .prompt([
    {
      type: 'list',
      name: 'fc',
      message: 'Select FC',
      choices: ['dfw1', 'mdt1', 'sdf1'],
    },
    {
      type: 'editor',
      name: 'packageIds',
      message: 'Paste ',
    },
  ])
  .then((answers) => {
    const wmPackageIds = parseText(answers.packageIds);
    const targetFc = answers.fc;
    status.start();
    fcPackageLookup(wmPackageIds, targetFc);
  });

const fcPackageLookup = (wmPackageIds, targetFc) => {
  const packageLookup = wmPackageIds.map(async (packageId, i) => {
    try {
      const record = await axios.get(
        `https://carrierservice-${targetFc}.supplychain.gstop-prod.com/v1/carrier/determine_postage/${packageId}/by_package_id`
      );

      const {
        wmPackageId,
        wmShipmentId = '',
        carrierTrackingId = '',
      } = record.data[0];

      return { wmPackageId, wmShipmentId, carrierTrackingId };
    } catch (e) {
      errors.push({ packageId, message: e.message });

      return {
        wmPackageId: packageId,
        wmShipmentId: '',
        carrierTrackingId: '',
      };
    }
  });

  Promise.all(packageLookup).then((results) => {
    results.forEach((result) => {
      const { wmPackageId, wmOrderId, carrierTrackingId } = result;
      infoOuput.push(
        `${wmPackageId},${wmOrderId ? wmOrderId : ''},${
          carrierTrackingId ? carrierTrackingId : ''
        }`
      );
      table.push([
        wmPackageId,
        wmOrderId ? wmOrderId : '',
        carrierTrackingId ? carrierTrackingId : '',
      ]);
    });
    status.stop();
    clear();
    console.log('\n\n\n');
    console.log(chalk.yellow(figlet.textSync('Sherlock', { width: '50' })));
    console.info(`Fulfillment Center: ${chalk.yellow.bold(targetFc)}\n`);
    console.info(table.toString());
    console.log(
      `\n\n${chalk.green.bold(
        'Filename'
      )}: \t${homedir}output-${targetFc}-${Date.now()}.csv \n${chalk.green.bold(
        'Records'
      )}: \t${packageLookup.length}\n${chalk.red.bold('Errors')}: \t${
        errors.length
      }\n\n`
    );

    fs.writeFileSync(
      `${homedir}output-${targetFc}-${Date.now()}.csv`,
      'wmPackageId,wmOrderId,trackingId\n' + infoOuput.join('\n').toString()
    );
  });
};
