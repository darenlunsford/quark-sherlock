## tl;dr

- Clone repo
- cd into dir
- npm i -g
- type sherlock
- IMPORTANT: Must be on VPN to use this

# Sherlock CLI

Sherlock CLI allows you to query wmPackageId and get back their wmShipmentID and carrierTrackingId. The query utilizes the Carrier Service API and is subject to changes that occur in the API.

## Using the CLI

You will need to have node installed.

## Commands

To run Sherlock just type sherlock from the terminal you will be prompted to select a specific FC run your query against.

![Sherlock Step One](/assets/step-one.png)

Past your new line seperated wmPackagIds when prompted.

![Sherlock Step Two](/assets/step-two.png)

```
56043204800272
56043204800293
56043204800683
56043204800811
56043204801909
56043204802319
56043204802362
56043304805501
56043304805542
```

The CLI will default to the editor you have specified in your profiles as EDITOR. Or prior to running the file you can export (example) EDITOR=nano

![Sherlock Step Three](/assets/step-three.png)

File will output to screen and download to the directory shown at the bottom of the screen.

![Sherlock Step Four](/assets/step-four.png)
